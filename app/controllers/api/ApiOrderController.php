<?php

class ApiOrderController extends BaseController {

    /**
     * @return string
     */
    public function saveOrder() {

        $request = Request::instance();

        $userOrder = new UserOrder;
        $userOrder->status = 'preparing';
        $userOrder->user_id = 1;
        $userOrder->qrcode = Coder::generate();
        $userOrder->products = json_encode($request->getContent());

        $userOrder->save();

        return Response::json(array(
            'status' => $userOrder->status,
            'code' => $this->generateQRcode($userOrder->qrcode)
        ));
    }

    /**
     * @param $code
     * @return string
     */
    private function generateQRcode($code) {

        return base64_encode(QrCode::format('png')->size(100)->generate($code));
    }
}