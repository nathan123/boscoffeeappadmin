<?php


class CashierOrderController extends BaseController {

    public function viewOrders() {

        $data = array(
            'orders' => UserOrder::getOrders()
        );
        if ( Auth::check() ) {
            return View::make('cashier.landing', $data );
        } 
        else {
            return Redirect::to('/');
        }
    }

    public function getOrders() {

        $data = array(
            'data' => UserOrder::getOrders()
        );

        return $data;
    }

    public function changeOrderStatus()
    {
        $orderId = Input::get('id');
        $status = Input::get('status');

        $userOrder = UserOrder::find( $orderId );
        $userOrder->status = $status;
        $userOrder->save();

        return Response::json( array(
            'code' => 200,
            'message' => 'Updated Successfully'
        ) );
    }

    public function viewSingleOrder( $orderId ) {

    }
}