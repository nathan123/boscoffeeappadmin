<?php

class LoginController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if( Auth::check() ) {
			return Redirect::to('dashboard');
		} else {
			return View::make('login');
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$data = array(
			'username'	=> 'required',
			'password'	=> 'required|alphaNum|min:4'
		);

		$validator = Validator::make(Input::all(), $data);

		if ( $validator->fails() ) {
			return Redirect::to('/')
				->withErrors( $validator )
				->withInput( Input::except('password') );
		}
		else {

			$userdata = array(
				'username'	=> Input::get('username'),
				'password'	=> Input::get('password')
			);

			if ( Auth::attempt( $userdata ) ) {
				return Redirect::to('dashboard');
			}
			else {

				return Redirect::to('/');
			}
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
