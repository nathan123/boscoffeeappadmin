<?php

class Coder {

    /**
     * @return int
     */
    public static function generate() {
        return mt_rand( 100000000000, 999999999999 );
    }
}