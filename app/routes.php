<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Route::get('/', function()
// {
// 	return View::make('hello');
// });

Route::get('/','LoginController@index');
Route::get('dashboard',function(){
	
	if( Auth::check() ){
		return View::make('dashboard');
	} 
	else {
		return Redirect::to('/');
	}

});

Route::get('users',function(){

	if( Auth::check() ){
		return View::make('users.users-list');
	} 
	else {
		return Redirect::to('/');
	}
});

Route::get('logout',function(){
	Auth::logout();
	return Redirect::to('/');
});

Route::resource('login', 'LoginController');

Route::group( array( 'prefix' => 'cashier' ), function() {

    Route::get('order/view', 'CashierOrderController@viewOrders');
    Route::get('order/data', 'CashierOrderController@getOrders');
    Route::post('order/change', 'CashierOrderController@changeOrderStatus');

});

Route::group( array( 'prefix' => 'api' ), function() {

    Route::post('order/save', 'ApiOrderController@saveOrder');
});

