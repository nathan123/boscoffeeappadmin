<?php

class UserOrder extends Eloquent {

    protected $table = 'user_orders';

    public static function getSingleOrder() {

    }

    public static function getOrders() {

        return DB::table('user_orders')
                 ->join('users', 'user_orders.user_id', '=', 'users.id')
                 ->select('user_orders.*', 'users.name', 'users.email' )
                 ->get();
    }
}