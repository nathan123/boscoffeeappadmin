<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::dropIfExists('user_orders');
        Schema::create('user_orders', function($table){
            $table->increments('id');
            $table->integer('user_id');
            $table->enum('status', array('preparing', 'ready', 'done')); // ready, preparing, done
            $table->text('products'); // json format
            $table->string('qrcode');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
