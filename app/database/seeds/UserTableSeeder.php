<?php

class UserTableSeeder extends Seeder {

    public function run() {

        User::create(
            array(
                'name' => 'John Doe',
                'email' => 'johndoe@example.com',
                'username' => 'john',
                'password' => Hash::make('john')

            )
        );
    }
}