<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">BOS COFFEE ADMIN</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown" style="border: 1px solid #ddd;"><i class="glyphicon glyphicon-user"></i>  Admin <strong class="caret"></strong></a>
                    <div class="dropdown-menu" style="padding: 15px; padding-bottom: 0px;">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#"><i class="glyphicon glyphicon-user"></i>  Profile</a></li>
                            <li><a href="#"><i class="glyphicon glyphicon-cog"></i>  Settings</a></li>
                            <li><a href="{{ URL::to('logout') }}"><i class="glyphicon glyphicon-lock"></i>  Logout</a></li>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
