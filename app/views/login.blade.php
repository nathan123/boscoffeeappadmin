@extends('defaults.default')
@section('content')	
	<div class="container">

		<div class="form-wrapper clearfix">
			<div class="row ">				
				<div class="logo-wrapper">
					{{ HTML::image('image/login-logo-img.png') }}
				</div>
				<div class="form-panel-box clearfix">					
					<div class="col-md-12">
						
						{{ Form::open(array('route'=>'login.store','class'=>'form-horizontal','id'=>'form-login')) }}
							<div class="form-group">
								<div class="col-md-12">
								 	{{  Form::macro('forminput', function($name, $class, $placeholder, $type){        
								     	return '<div class="inner-addon left-addon">
								             <i class="glyphicon '.$class.'"></i>
								             <input type="'.$type.'" name="'.$name.'" class="form-control" placeholder="'.$placeholder.'" />
								             </div>';
								     	}); 
								  	}}
								  	{{ Form::forminput('username', 'glyphicon-user', 'Username','text') }}
                                    @if( $errors->first('username') )
                                        {{ $errors->first('username') }}
                                    @endif
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									{{ Form::forminput('password', 'glyphicon-lock', 'Password','password') }}
                                    @if( $errors->first('password') )
                                        {{ $errors->first('password') }}
                                    @endif
								</div>
							</div>	
									{{  Form::macro('formsubmit', function($name, $class, $value){        
								     	return '<div class="inner-addon submit-addon">
								            	<input type="submit" name="'.$name.'" class="btn col-md-12" value="'.$value.'" />
								            	<i class="glyphicon '.$class.'"></i>
								             	</div>';
								     	}); 
								  	}}
									{{ Form::formsubmit('submit','glyphicon-circle-arrow-right','Sign in') }}		

						{{ Form::close() }}
						<div class="form-group">
							<div class="col-md-12">
								<a href="#">Forgot password?</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop