<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="/favicon.png">
    <!-- <link rel="shortcut icon" href="{{ asset('favicon.png') }}"> -->
<!-- load bootstrap from a cdn -->
    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('css/bootstrap-theme.min.css') }}
    {{ HTML::style('css/font-awesome.min.css') }}
    {{ HTML::style('css/main.css') }}
    {{ HTML::style('css/boschart.min.css') }}

    {{ HTML::script('js/modernizr-2.8.3-respond-1.4.2.min.js') }}
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.css">


</head>
<body>  
        @if( Auth::check() )
		  @include('includes.header')
        @endif

		<div class="container-fluid">
            @yield('content')
    	</div>

        <div class="text-center">
            @include('includes.footer')    	
        </div>
    	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        {{ HTML::script('js/bootstrap.min.js') }}
        {{ HTML::script('js/main.js') }}
        {{ HTML::script('js/boschart.min.js') }}
        <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.js"></script>
        <script type="text/javascript">

            function changeStatus( id, status ) {
                $.post( "{{ URL::to('cashier/order/change') }}", { id: id, status: status})
                        .done(function( data ) {
                            alert( 'Updated Successfully');
                        });
            }

            $(document).ready(function() { 

                var path = "{{ URL::to('cashier/order/data')  }}"; 

                $('#orders').dataTable( {
                    "ajax": path,
                    "columns": [
                        { "data": "id" },
                        { "data": "name" },
                        { "data": "" },
                        { "data": "status" },
                        { "data": "created_at" },
                        { "data": "qrcode" }
                    ],
                    "columnDefs": [
                        {
                            // The `data` parameter refers to the data for the cell (defined by the
                            // `data` option, which defaults to the column being worked with, in
                            // this case `data: 0`.
                            "render": function ( data, type, row ) {
                                return "<a href='#'>view</a>";
                            },
                            "targets": 2
                        },
                        {
                            // The `data` parameter refers to the data for the cell (defined by the
                            // `data` option, which defaults to the column being worked with, in
                            // this case `data: 0`.
                            "render": function ( data, type, row ) {

                                var preparing = '';
                                var ready = '';
                                var done = '';


                                if( data == 'preparing' ) {
                                    preparing = 'selected';
                                }
                                else if( data == 'ready' ) {
                                    ready = 'selected';
                                }
                                else {
                                    done = 'selected';
                                }

                                return "<select name='status' id='" + row['id'] + "' onchange='changeStatus(this.id, this.value)'>" +
                                           "<option value='preparing' " + preparing + ">Preparing</option>" +
                                           "<option value='ready' " + ready + ">Ready</option>" +
                                           "<option value='done' " + done + ">Done</option>" +
                                        "</select>";
                            },
                            "targets": 3
                        },
                    ]
                }); 
            });     

        </script>
</body>
</html>