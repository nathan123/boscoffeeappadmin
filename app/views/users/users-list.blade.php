@extends('defaults.default')
@section('content')
	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="{{ URL::to('dashboard') }}"><i class="glyphicon glyphicon-home"></i>  Dashboard<span class="sr-only">(current)</span></a></li>
            <li><a href="{{ URL::to('cashier/order/view') }}"><i class="glyphicon glyphicon-cutlery"></i>  Merchant Info</a></li>
            <li class="active"><a href="{{ URL::to('users') }}"><i class="glyphicon glyphicon-user"></i>  Customers</a></li>
            <li><a href="#"><i class="glyphicon glyphicon-calendar"></i>  Table</a></li>
          </ul>
        </div>
        <div class="col-sm-12 col-sm-offset-3 col-md-10 col-md-offset-2 breadcrumbs">
             <h2 class="">User</h2>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="section-wrapper">
                <table id="users" class="display text-center" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="text-center" width="20%">Customers Name</th>
                        <th class="text-center" width="20%">Address</th>
                        <th class="text-center" width="20%">Number</th>
                        <th class="text-center" width="20%">Loyalty Points</th>
                        <th class="text-center" width="20%">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    	<tr>
                    		<td>John Doe</td>
                    		<td>New York</td>
                    		<td>123456</td>
                    		<td>100</td>
                    		<td><span style="color: green;">Active</span></td>
                    	</tr>
                    	<tr>
                    		<td>John Doe</td>
                    		<td>New York</td>
                    		<td>123456</td>
                    		<td>100</td>
                    		<td><span style="color: green;">Active</span></td>
                    	</tr>
                    	<tr>
                    		<td>John Doe</td>
                    		<td>New York</td>
                    		<td>123456</td>
                    		<td>100</td>
                    		<td><span style="color: green;">Active</span></td>
                    	</tr>
                    	<tr>
                    		<td>John Doe</td>
                    		<td>New York</td>
                    		<td>123456</td>
                    		<td>100</td>
                    		<td><span style="color: green;">Active</span></td>
                    	</tr>
                    	<tr>
                    		<td>John Doe</td>
                    		<td>New York</td>
                    		<td>123456</td>
                    		<td>100</td>
                    		<td><span style="color: green;">Active</span></td>
                    	</tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop