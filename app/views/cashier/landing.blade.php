@extends('defaults.default')
@section('content')
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="{{ URL::to('dashboard') }}"><i class="glyphicon glyphicon-home"></i>  Dashboard<span class="sr-only">(current)</span></a></li>
            <li class="active"><a href="{{ URL::to('cashier/order/view') }}"><i class="glyphicon glyphicon-cutlery"></i>  Merchant Info</a></li>
            <li><a href="{{ URL::to('users') }}"><i class="glyphicon glyphicon-user"></i>  Customers</a></li>
            <li><a href="#"><i class="glyphicon glyphicon-calendar"></i>  Table</a></li>
          </ul>
        </div>
        <div class="col-sm-12 col-sm-offset-3 col-md-10 col-md-offset-2 breadcrumbs">
             <h2 class="">Order List</h2>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="section-wrapper">
                <h4>New Order List For Today June 01, 2015</h4>
                <table id="orders" class="display" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Order ID</th>
                        <th>Customer</th>
                        <th>Product</th>
                        <th>Status</th>
                        <th>Order date</th>
                        <th>Code</th>
                    </tr>
                    </thead>

                    <tfoot>
                    <tr>
                        <th>Order ID</th>
                        <th>Customer</th>
                        <th>Product</th>
                        <th>Status</th>
                        <th>Order date</th>
                        <th>Code</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>    
@stop